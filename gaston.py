import sys
import speech_recognition as sr
import espeakng
import pywhatkit
import datetime
import wikipedia
import pyjokes
import os
import requests
import pyaudio
import psutil
import subprocess
from time import sleep
import vlc
import pafy
import easygui


listener = sr.Recognizer()
speaker = espeakng.Speaker()
speaker.pitch = 55
speaker.wpm = 175
speaker.amplitude = 180
wikipedia.set_lang("fr")
speaker.voice = 'fr'

def period():
    currentH = int(datetime.datetime.now().hour)
    if currentH >= 0 and currentH < 12:
        talk('Bonjour!')

    if currentH >= 12 and currentH < 18:
        talk('Bonne après-midi!')

    if currentH >= 18 and currentH != 0:
        talk('Bonsoir!')
    year = int(datetime.datetime.now().year)
    month = int(datetime.datetime.now().month)
    date = int(datetime.datetime.now().day)
    time = datetime.datetime.now().strftime('%H:%M')
    talk("Je m'appelle Gaston, je suis la pour vous aider au quotidien")
    print(time + (" ") + str(date) + ("/") + str(month) + '/' + str(year))

def cpu():
    usage = str(psutil.cpu_percent())
    talk('Le CPU est à' + usage)
    print(usage)

def talk(text):
    speaker.say(text, wait4prev=True)
    speaker.wait()

def take_command():
    try:
        r = sr.Recognizer()
        with sr.Microphone(device_index=4) as source:
            print("à l'écoute ...")
            r.pause_threshold = 1
            voice = listener.listen(source)
            command = listener.recognize_google(voice, language="fr-FR")
            command = command.lower()
            if 'gaston' in command:
                command = command.replace('gaston', '')
                print(command)
    except sr.UnknownValueError:
        print("Could not understand audio")
        command = str(input('commande:'))
        pass
    return command

def bye():
    talk('Ok, à bientot')
    sys.exit()
def blague():
    talk(pyjokes.get_joke(language='fr'))    
def joue():
    song = command.replace('joue','')
    talk('joue '+ song)
    pywhatkit.playonyt(song)
def who():
    person = command.replace('qui est', '')
    info = wikipedia.summary(person, 1)
    print(info)
    talk(info)
def pendule():
    time = datetime.datetime.now().strftime('%H:%M')
    talk('il est actuellement  ' + time)   
def meteo():
     url = ''  # Open api link here
     res = requests.get(url)
     data = res.json()

     weather = data['weather'][0]['main']
     temp = data['main']['temp']
     wind_speed = data['wind']['speed']

     latitude = data['coord']['lat']
     longitude = data['coord']['lon']

     description = data['weather'][0]['description']
     talk('Temperature : {} degree celcius'.format(temp))
     print('Wind Speed : {} m/s'.format(wind_speed))
     print('Latitude : {}'.format(latitude))
     print('Longitude : {}'.format(longitude))
     print('Description : {}'.format(description))
     print('weather is: {} '.format(weather))
     talk('la météo est : {} '.format(description))
def Thanks():
    talk('de rien maitre')
def range():
    import sort
    subprocess.CompletedProcess(args=sort, returncode=0)
    speaker.wait()
def playlist():
    
    url = "https://www.youtube.com/watch?v=OntcAJYG29A"
    video = pafy.new(url)
    best = video.getbestaudio()
    media = vlc.MediaPlayer(best.url)
    while True:
        choice = easygui.buttonbox(title="VL Moche",msg="Press Play to start",choices=["Play","Pause","Stop"])
        print(choice)
        if choice == "Play":
            media.play()
        elif choice == "Pause":
            media.pause()
        elif choice == "Stop":
            media.stop()
            break
        else:
            break



if __name__ == "__main__":
    period()
    while True:
        command = take_command()
        print(command)

        if 'blague' in command:
            blague()
        
        elif 'joue' in command:
            joue()

        elif 'qui est' in command:
            who()

        elif "donne-moi l'heure" in command:
            pendule()
        elif "lave-vaisselle" and "vaisselle" in command:
            range()
            
        elif 'quelle est la météo' and 'météo' in command:
            meteo()

        elif 'merci' in command:
            Thanks()

        elif 'cpu' in command:
            cpu()
        elif"playlist" in command:
            playlist()
        elif 'stoppe toi' in command:
            bye()
    else:
        talk("répéter svp")
        take_command()
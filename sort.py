"""

sort.py

This script is used to list the files in the Downloads folder and then sort them into the correct folders according to their extensions

Author: BigoudOps bigoudops.fr

"""

import os
import subprocess
import shutil
import glob
from os import path
import pathlib
from gi.repository import GLib
# calling XDG Directories for getting right name of user folders

Folder_User = GLib.get_home_dir()
Folder_Document = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_DOCUMENTS)
Folder_Download = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_DOWNLOAD)
Folder_Pictures = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_PICTURES)
Folder_Templates = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_TEMPLATES)
Folder_Music = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)
Folder_Public_Share = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE)
Folder_Videos = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS)

ExtensionDocuments = ['.txt', '.pdf', '.odt', '.docx', '.wps']
ExtensionPictures = ['.jpg', '.png', '.gif', '.jpeg', '.xcf', '.svg', ]
ExtensionMusic = ['.aac', '.flac', '.ogg', '.mp3', '.wav', '.m3u']
ExtensionVideo = ['.avi', '.3gp', '.mkv', '.mov', '.mp4', '.webm']
ExtensionSoftware = ['.deb', '.tar', '.gz', '.zip', '.xz']
ExtensionISO = ['.iso', '.img', '.vmdk', '.qcow2']
ExtensionBDD = ['.kdb', '.kdbx']
ExtensionAndroid = ['.apk']
ExtensionPsVita = ['.vpk']
ExtensionVSCodium = ['.vsix']

print("Your folder names are ")
print(Folder_User)
print(Folder_Download + "           " + Folder_Pictures)
print(Folder_Document + "           " + Folder_Music)
print(Folder_Public_Share + "           " + Folder_Videos)
print()
print()
print("Now i'm gonna list files in the folder " + Folder_Download)
print()
print()
print(os.listdir(Folder_Download))

softwareFiles = os.path.join(Folder_Document, 'ProgramFiles')
iso = os.path.join(Folder_Document, 'ISO')
documentsFiles = os.path.join(Folder_Document,)
pictureFiles = os.path.join(Folder_Pictures,)
musicFiles = os.path.join(Folder_Music,)
videoFiles = os.path.join(Folder_Videos,)
bddFiles = os.path.join(Folder_User, 'BDD')
vpkFiles = os.path.join(Folder_User, 'PSVITA')
androidFiles = os.path.join(Folder_User, 'apkAndroid')
vsixFiles = os.path.join(Folder_User, 'VsixCODIUM')
files = glob.glob(os.path.join(Folder_Download, '*'))

for file in files:
    extension = os.path.splitext(file)[1].lower()
    if extension in ExtensionSoftware:
        if not path.exists(softwareFiles):
            os.mkdir(softwareFiles)
        shutil.move(file, softwareFiles)
    if extension in ExtensionVideo:
        if not path.exists(videoFiles):
            os.mkdir(videoFiles)
        shutil.move(file, videoFiles)
    if extension in ExtensionMusic:
        if not path.exists(musicFiles):
            os.mkdir(musicFiles)
        shutil.move(file, musicFiles)
    if extension in ExtensionDocuments:
        if not path.exists(documentsFiles):
            os.mkdir(documentsFiles)
        shutil.move(file, documentsFiles)
    if extension in ExtensionISO:
        if not path.exists(iso):
            os.mkdir(iso)
        shutil.move(file, iso)
    if extension in ExtensionPictures:
        if not path.exists(pictureFiles):
            os.mkdir(pictureFiles)
        shutil.move(file, pictureFiles)
    if extension in ExtensionBDD:
        if not path.exists(bddFiles):
            os.mkdir(bddFiles)
        shutil.move(file, bddFiles)
    if extension in ExtensionAndroid:
        if not path.exists(androidFiles):
            os.mkdir(androidFiles)
        shutil.move(file, androidFiles)
    if extension in ExtensionPsVita:
        if not path.exists(vpkFiles):
            os.mkdir(vpkFiles)
        shutil.move(file, vpkFiles)
    if extension in ExtensionVSCodium:
        if not path.exists(vsixFiles):
            os.mkdir(vsixFiles)
        shutil.move(file, vsixFiles)
print()
print("you still have these files to sort manually")
print()
print(os.listdir(Folder_Download))

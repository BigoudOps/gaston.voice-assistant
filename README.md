# About :

## FRANÇAIS:
Ceci est un ensemble de script python créant un assistant vocale donnant plusieurs information plus ou moins utiles. Il est développée sur Ubuntu 20.04 avec comme paramètre vocale le Français.

La librairie pyjokes est modifié avec l'ajout de blague en français présente dans le depot github : [Github/alfreddagenais](https://github.com/alfreddagenais/pyjokes/tree/french "Github/alfreddagenais")

Vous avez également besoin de espeak-ng d'installer sur votre ordinateur. Pour Ubuntu 20.04 : `sudo apt install espeak-ng`

En cas de problème pour installer le module PyAudio sur Ubuntu ➡ `sudo apt install portaudio19-dev `

Pour faciliter l'installation des modules nécessaire au bon fonctionnement de Gaston, veuillez utiliser la commande `pip3 install -r requirements.txt`.

Les dépendances python sont :
- [espeak-ng](https://pypi.org/project/espeakng/ "https://pypi.org/project/espeakng/")
- [SpeechRecognition](https://pypi.org/project/SpeechRecognition/ "https://pypi.org/project/SpeechRecognition/")
- [pywhatkit](https://pypi.org/project/pywhatkit/ "https://pypi.org/project/pywhatkit/")
- [wikipedia](https://pypi.org/project/wikipedia/ "https://pypi.org/project/wikipedia/")
- [pyjokes](https://pypi.org/project/pyjokes/ "https://pypi.org/project/pyjokes/")
- [pafy](https://pypi.org/project/pafy/ "https://pypi.org/project/pafy/")
- [pyaudio](https://pypi.org/project/PyAudio/ "https://pypi.org/project/PyAudio/")
- [easygui](https://pypi.org/project/easygui/ "https://pypi.org/project/easygui/")
- [python-vlc](https://pypi.org/project/python-vlc/ "https://pypi.org/project/python-vlc/")
  
Pour sélectionner un micro dans votre environnement, veuillez utiliser le script **testmicro** qui vous retourne une liste de possibilités propre a votre configuration. **Attention** le retour du script est une liste en python donc le premier résultat est donc __device_index0__. Puis reportez le à la ligne `with sr.Microphone(device_index=4) as source:`. Si vous désirez utiliser le micro intégrée à votre ordinateur vous pouvez simplement supprimer l'argument device index.

![capture](src/test.gif)
 
Pour la météo il faut renseigner une api [openweathermap](openweathermap.org "openweathermap.org") puis l'intégrée à la première ligne dans la fonction ( meteo(): )  

## ENGLISH:
This is a set of python scripts creating a voice assistant giving several more or less useful information. It is developed on Ubuntu 20.04 with the French voice parameter.

You also need espeak-ng to install on your computer. For Ubuntu 20.04: `sudo apt install espeak-ng`

If you have a problem installing the PyAudio module on Ubuntu ➡ `sudo apt install portaudio19-dev` 

To facilitate the installation of the modules necessary for the proper functioning of Gaston, please use the command `pip3 install -r requirements.txt`.

The python dependencies are:
- [espeak-ng](https://pypi.org/project/espeakng/ "https://pypi.org/project/espeakng/")
- [SpeechRecognition](https://pypi.org/project/SpeechRecognition/ "https://pypi.org/project/SpeechRecognition/")
- [pywhatkit](https://pypi.org/project/pywhatkit/ "https://pypi.org/project/pywhatkit/")
- [wikipedia](https://pypi.org/project/wikipedia/ "https://pypi.org/project/wikipedia/")
- [pyjokes](https://pypi.org/project/pyjokes/ "https://pypi.org/project/pyjokes/")
- [pafy](https://pypi.org/project/pafy/ "https://pypi.org/project/pafy/")
- [pyaudio](https://pypi.org/project/PyAudio/ "https://pypi.org/project/PyAudio/")
- [easygui](https://pypi.org/project/easygui/ "https://pypi.org/project/easygui/")
- [python-vlc](https://pypi.org/project/python-vlc/ "https://pypi.org/project/python-vlc/")

To select a microphone in your environment, please use the **testmicro** script which returns a list of possibilities specific to your configuration.**Be careful**, the return of the script is a list in python so the first result is __device_index0__. Then transfer it to the line `with sr.Microphone (device_index = 4) as source :`. If you want to use the microphone built into your computer you can simply remove the device index argument.

![capture](src/test.gif)

For the weather, you have to enter an api [openweathermap](openweathermap.org "openweathermap.org") then integrate it into the first line in the function ( meteo (): )